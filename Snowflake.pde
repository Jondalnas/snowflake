ArrayList<Integer> waterX;
ArrayList<Integer> waterY;
ArrayList<Integer> waterState;

HashMap<Integer, ArrayList<Integer[]>> frozen;

PImage backgroundImage;

void setup() {
  //size(640, 480);
  //fullScreen();
  
  backgroundImage = loadImage("background.png");
  
  size(640, 480);
  
  frameRate(1000);
  
  waterX = new ArrayList<Integer>();
  waterY = new ArrayList<Integer>();
  waterState = new ArrayList<Integer>();
  frozen = new HashMap<Integer, ArrayList<Integer[]>>();
  
  for (int x = 0; x < width; x++) {
    frozen.put(x, new ArrayList<Integer[]>());
  }
  
  for (int i = 0; i < width*height; i++) {
    if (random(1) > 0.1) continue;
    
    int xx = i % width;
    int yy = i / width;
    
    waterState.add(1);
    waterX.add(xx);
    waterY.add(yy);
  }
  
  for (int i = 0; i < backgroundImage.pixels.length; i++) {
    int col = backgroundImage.pixels[i] & 0xFFFFFF;
    
    col = ((col >> 16) & 0xFF + (col >> 8) & 0xFF + col & 0xFF);
    
    int x = i % backgroundImage.width;
    int y = i / backgroundImage.width;
    
    if (col > 50) {
      waterState.add(2);
      waterX.add(x);
      waterY.add(y);
      addFrozen(x, y);
    }
  }
  
  /*for (int x = 0; x < width; x++) {
    waterState.add(2);
    waterX.add(x);
    waterY.add(0);
    addFrozen(x, 0);
    
    waterState.add(2);
    waterX.add(x);
    waterY.add(height-1);
    addFrozen(x, height-1);
  }
  
  for (int y = 0; y < width; y++) {
    waterState.add(2);
    waterX.add(0);
    waterY.add(y);
    addFrozen(0, y);
    
    waterState.add(2);
    waterX.add(width-1);
    waterY.add(y);
    addFrozen(width-1, y);
  }
  
  waterState.add(2);
  waterX.add(width/2);
  waterY.add(height/2);
  addFrozen(width/2, height/2);*/
}

void draw() {
  background(0);
  for (int i = 0; i < waterState.size(); i++) {
    if (waterState.get(i) == 1) {
      
      int dir = (int) (random(4));
      
      switch(dir) {
        case 0:
          move(i, 0, -1);
          break;
          
        case 1:
          move(i, 1, 0);
          break;
          
        case 2:
          move(i, 0, 1);
          break;
          
        case 3:
          move(i, -1, 0);
          break;
      }
    }
  }
  
  for (int i = 0; i < waterState.size(); i++) {
    
    if (waterState.get(i) == 0) continue;
  
    int x = waterX.get(i);
    int y = waterY.get(i);
    
    set(x, y, waterState.get(i) == 1 ? color(0,0,255) : color(255));
  }
}

void move(int index, int x, int y) {
  int xx = waterX.get(index);
  int yy = waterY.get(index);
  
  if (xx+x < 0 || xx+x >= width || yy+y < 0 || yy+y >= height) return;
  
  if (collides(xx+x, yy+y)) {
    waterState.set(index, 2);
    addFrozen(xx, yy);
  } else {
    waterX.set(index, xx+x);
    waterY.set(index, yy+y);
  }
}

boolean collides(int x, int y) {
  for (int i = 0; i < frozen.get(x).size(); i++) {
    if (((ArrayList<Integer[]>) frozen.get(x)).get(i)[1] == y) {
      return true;
    }
  }
  
  return false;
}

void addFrozen(int x, int y) {
  if (frozen.get(x) != null) {
    ((ArrayList<Integer[]>) frozen.get(x)).add(new Integer[]{x, y});
  } else {
    frozen.put(x, new ArrayList<Integer[]>());
    ((ArrayList<Integer[]>) frozen.get(x)).add(new Integer[]{x, y});
  }
}
